void main() {
  // print("Hello World!");
  // print("Hello World!");
  // print("Hello World!");
  // print("Hello World!");

  final name = "Rachel Ardana Putra Ginting";

  print(name);
  print(name);
  print(name);
  print(name);
  print(name);

  var firstName = "Dinda";
  final lastName = "Indriana";

  // lastName = "Fitria";

  print(firstName);
  print(lastName);

  final array1 = [1, 2, 3];
  const array2 = [1, 2, 3];

  print(array1);
  print(array2);

  late var value = getValue();
  print('Variabel sudah dibuat');
  print(value);
}

String getValue() {
  print('getValue() di panggil');
  return 'Rachel Ginting';
}
