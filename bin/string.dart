void main() {
  String firstName = "Rachel";
  String lastName = "Ginting";

  print(firstName);
  print(lastName);

  var fullName = '$firstName ${lastName}';

  print(fullName);

  var name1 = firstName + " " + lastName;
  var name2 = 'Rachel ' 'Ginting';

  print(name1);
  print(name2);

  var longString = '''
----
rachelginting
pajarginting
fatanginting
----
''';

  print(longString);
}
